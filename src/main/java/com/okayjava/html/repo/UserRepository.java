package com.okayjava.html.repo;

import org.springframework.data.repository.CrudRepository;

import com.okayjava.html.model.User;

public interface UserRepository extends CrudRepository<User, Integer>{

}
