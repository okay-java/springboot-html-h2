# springboot-html-h2

Springboot with HTML from and H2 Database 


## Getting started

Springboot with HTML form and H2 database 

1. Create spring boot project 
2. Create pur HTML page to take user input 
3. Set up H2 database configuration details in application.properties
4. Create UserRepositoy Interface and extend CrudRepositoy Interface
5. Use UserRepositoy in UserController.
6. Goto h2 console and verify the data in database
